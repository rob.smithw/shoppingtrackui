import { Component } from "@angular/core";
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { Switch } from "tns-core-modules/ui/switch";
import { ActivatedRoute, Router } from '@angular/router';
import { request, getJSON, getString } from "tns-core-modules/http";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Observable } from 'rxjs';
import { ApiService } from '../shared/services/api.service';
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Store } from "~/app/shared/models/store.component";
import { User } from "~/app/shared/models/user.component";
import { Constants } from "~/app/shared/constants/constants.component";

@Component({
    selector: 'store-select',
    templateUrl: './storeSelection.component.html',
    styleUrls: ['./storeSelection.component.css'],
    moduleId: module.id
})


export class StoreSelect {
    shared: Constants;
    errorMessage: string;
    stores: Store[];
    allStores: Store[];
    allStoresWithItems: Store[];
    currentUser: User;
    redirect: Redirects;
    showStoreList: boolean = false;
    storeNameList: Array<string>;
    selectedStoreInd: number = 0;
    selectedStoreName: string = "";
    storeToOpenName: string;
    showAllStores: boolean = false;

    constructor(private route: ActivatedRoute, private router: RouterExtensions, private api_service: ApiService){
        this.route.queryParams.subscribe(params => {
            this.currentUser = JSON.parse(params["user"]);
        });
    }

    async ngOnInit() {
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        await this.GetStoresWithItems();
    }

    async GetStoresWithItems() {
        await this.api_service.getStoresWithItems(this.currentUser.user_Id).toPromise()
        .then( (response: Store[]) => {
            var sortedList = response.sort((first: Store, second: Store) => {
                if(first.name > second.name){
                    return 1;
                }
                if(first.name < second.name){
                    return -1;
                }
                return 0;
            });
            this.allStoresWithItems = sortedList;
            this.stores = sortedList;
        });
    }

    ShowItemsForStores(): void {
        this.redirect.Home
    }

    RedirectToItems(store: Store): void{
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(store)
            }
        };
        this.redirect.Home(navigationExtras)
    }

    GetAllStores(): void{
        this.api_service.getAllStores()
            .subscribe((response: Store[]) => {
                this.stores = response.sort((first: Store, second: Store) => {
                    if(first.name > second.name){
                        return 1;
                    }
                    if(first.name < second.name){
                        return -1;
                    }
                    return 0;
                });
            },
            error => {
                console.error(error);
            });
    }

    ToggleShowAllStores(args: EventData) {
        let sw = args.object as Switch;
        let isChecked = sw.checked;
        if (isChecked){
            this.GetAllStores();
        }
        else{
            this.stores = this.allStoresWithItems;
        }
    }

    OpenStoreSelect(): void{
        if(this.shared.isUndefinedOrNull(this.storeNameList) || this.storeNameList === []){
            this.CreateListWithNames();
        }
        if(!this.shared.isUndefinedOrNull(this.storeToOpenName) && this.storeToOpenName !== ''){
            this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.storeToOpenName, this.storeNameList);
        }
        this.showStoreList = true;
    }

    CreateListWithNames(): void{
        this.storeNameList = [];
        //initialize list
        if(!this.shared.isUndefinedOrNull(this.stores)){
            this.stores.forEach(store => {
                this.storeNameList.push(store.name);
            });
        }
        //sort list
        if(!this.shared.isUndefinedOrNull(this.storeNameList)){
            this.storeNameList = this.storeNameList.sort((first: string, second: string) => {
                if(first > second){
                    return 1;
                }
                if(first < second){
                    return -1;
                }
                return 0;
            });
        }
    }

    

    OnSelectedIndexChanged(args: EventData) {
        const picker = <ListPicker>args.object;
        this.selectedStoreInd = picker.selectedIndex;
        this.selectedStoreName = this.storeNameList[picker.selectedIndex];
    }

    SaveStoreSelection(): void{
        this.CloseStoreSelect();
        this.storeToOpenName = this.selectedStoreName;
    }

    CloseStoreSelect(): void{
        this.showStoreList = false;
    }
}