import { Component } from "@angular/core";
import { Constants } from "../shared/constants/constants.component";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Redirects } from "../shared/constants/redirects.component";
import { User } from "../shared/models/user.component";
import { ApiService } from "../shared/services/api.service";

@Component({
    selector: 'LoginComponent',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    moduleId: module.id
})


export class LoginComponent {

    shared: Constants;
    userRoute: string = "User";
    registerPage: boolean = false;
    redirect: Redirects;
    confirmPass: string = "";
    signInUser: User;
    registeringUser: User;
    currentUser: User;
    errorMessage: string;

    public constructor(private router: RouterExtensions, private api_service: ApiService){

    }

    ngOnInit(): void {
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        this.signInUser = new User();
        this.currentUser = new User();
        this.registeringUser = new User();
    }

    renderRegisterPage(): void{
        this.registerPage = true;
    }

    renderSignInPage(): void{
        this.registerPage = false;
    }

    signIn(username: string, password: string): void{
        if(username == "" || password == ""){
            if(username == ""){
                this.errorMessage = "Please enter username.";
                return;
            }
            if(password == ""){
                this.errorMessage = "Please enter password.";
                return;
            }
        }

        let attemptedLogin: User = new User(username, password);
        this.api_service.login(attemptedLogin)
            .subscribe(
                (response: User) => {
                    this.currentUser = response;
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            "user": JSON.stringify(this.currentUser)
                        }
                    };
                    this.redirect.StoreSelect(navigationExtras);
                },
                error => {
                    this.signInUser.password = "";
                    this.errorMessage = error;
                    console.log(error);
                }
            );
    }

    registerUser(email: string, username: string, password: string, confirm: string): void{
        this.errorMessage = "";

        if(email == "" || username == "" || password == "" || confirm == ""){
            if(email == ""){
                this.errorMessage = "Email is required.";
                return;
            }
            if(username == ""){
                this.errorMessage = "Username is required.";
                return;
            }
            if(password == ""){
                this.errorMessage = "Password is required.";
                return;
            }
            if(confirm == ""){
                this.errorMessage = "Please confirm password.";
                this.registeringUser.password = "";
                return;
            }
        }

        if(password != confirm){
            this.errorMessage = "Passwords do not match.";
            this.registeringUser.password = "";
            this.confirmPass = "";
            return;
        }

        let attemptedRegister: User = new User(username, password, email);
        this.api_service.registerUser(attemptedRegister)
            .subscribe(
                (response: User) => {
                    this.registeringUser = new User();
                    this.confirmPass = "";
                    this.renderSignInPage();
                },
                error => {
                    this.errorMessage = error;
                }
            );
    }

}
