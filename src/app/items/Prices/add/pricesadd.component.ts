import { Component } from "@angular/core";
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import {ActivatedRoute, Router} from '@angular/router';
import { request, getJSON, getString } from "tns-core-modules/http";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Item, Details } from "~/app/shared/models/item.component";
import { DatePipe } from '@angular/common';
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Store } from "~/app/shared/models/store.component";
import { User } from "~/app/shared/models/user.component";
import { Constants } from "~/app/shared/constants/constants.component";
import { Price } from "~/app/shared/models/price.component";

@Component({
    selector: 'prices-add',
    templateUrl: './pricesadd.component.html',
    styleUrls: ['./pricesadd.component.css'],
    moduleId: module.id
})


export class PricesAdd {

    shared: Constants;
    result: Item;
    newPrice: Price;
    errorMessage: string;
    newStoreName: string;
    stores: Store[];
    displayDateString: string;
    showDatePicker: boolean = false;
    currentUser: User;
    currentStore: Store;
    redirect: Redirects;
    datePickerDate: Date;
    markAsPreviousPrice: boolean = false;
    showStoreList: boolean = false;
    storeNameList: Array<string>;
    selectedStoreInd: number = 0;
    selectedStoreName: string;

    constructor(private route: ActivatedRoute, private router: RouterExtensions, private datePipe: DatePipe){
        this.route.queryParams.subscribe(params => {
            this.result = JSON.parse(params["item"]);
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void{
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        this.newPrice = new Price();
        this.displayDateString = this.datePipe.transform(this.newPrice.dateOfPrice, 'MM-dd-yyyy');
        this.GetAllStores();
    }

    AddPrice(): void{
        if(!this.shared.isUndefinedOrNull(this.newStoreName)){
            this.newPrice.dateOfPrice = new Date(this.displayDateString);
            this.newPrice.storeId = this.shared.getStoreIdByName(this.newStoreName, this.stores);
            if(this.newPrice.storeId === 0){
                this.errorMessage = "Store name could not be found.";
            }
            if(!this.shared.isUndefinedOrNull(this.newPrice) && this.newPrice.storeId > 0){
                request({
                    url: this.shared.SHOPPINGTRACKTEST_API + "Prices",
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    content: JSON.stringify({
                        itemId: this.result.itemId,
                        userId: this.result.user_Id,
                        price: this.newPrice.price,
                        storeId: this.newPrice.storeId,
                        dateOfPrice: this.newPrice.dateOfPrice
                    })
                }).then((response) => {
                }, (e) => {
                    this.errorMessage = e;
                    //attempt to log this error
                });

                if(this.markAsPreviousPrice){
                    request({
                        url: this.shared.SHOPPINGTRACKTEST_API + "Items/" + this.result.itemId,
                        method: "PUT",
                        headers: { "Content-Type": "application/json" },
                        content: JSON.stringify({
                            itemId: this.result.itemId,
                            user_id: this.result.user_Id,
                            name: this.result.name,
                            previous_price: this.newPrice.price,
                            last_store_id: this.newPrice.storeId,
                            deleted: this.result.deleted,
                            purchased: this.result.purchased
                        })
                    }).then((response) => {
                        this.result.previous_Price = this.newPrice.price;
                        this.result.last_Store_Id = this.newPrice.storeId;
                        this.RedirectToDetails();
                    }, (e) => {
                        this.errorMessage = e;
                        //attempt to log this error
                    });
                }
                else{
                    //check if error message is empty
                    this.RedirectToDetails();
                }
            }
        }
        else{
            this.errorMessage = "Store name is required.";
        }

    }

    GetAllStores(){
        this.stores = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API + "Stores").then((r: any) => {
            this.stores = r;
        }, (e) => {
        });
    }

    OpenStoreSelect(): void{
        if(this.shared.isUndefinedOrNull(this.storeNameList) || this.storeNameList === []){
            this.CreateListWithNames();
        }
        if(!this.shared.isUndefinedOrNull(this.newStoreName) && this.newStoreName !== ''){
            this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.newStoreName, this.storeNameList);
        }
        this.showStoreList = true;
    }

    CreateListWithNames(): void{
        this.storeNameList = [];
        //initialize list
        if(!this.shared.isUndefinedOrNull(this.stores)){
            this.stores.forEach(store => {
                this.storeNameList.push(store.name);
            });
        }
        //sort list
        if(!this.shared.isUndefinedOrNull(this.storeNameList)){
            this.storeNameList = this.storeNameList.sort((first: string, second: string) => {
                if(first > second){
                    return 1;
                }
                if(first < second){
                    return -1;
                }
                return 0;
            });
        }
    }

    OnSelectedIndexChanged(args: EventData) {
        const picker = <ListPicker>args.object;
        this.selectedStoreInd = picker.selectedIndex;
        this.selectedStoreName = this.storeNameList[picker.selectedIndex];
    }

    SaveStoreSelection(): void{
        this.CloseStoreSelect();
        this.newStoreName = this.selectedStoreName;
    }

    CloseStoreSelect(): void{
        this.showStoreList = false;
    }

    CloseDatePicker(): void{
        this.showDatePicker = false;
    }

    OpenDatePicker(): void{
        this.datePickerDate = new Date(this.displayDateString);
        this.showDatePicker = true;
    }

    SaveDatePicker(): void{
        this.showDatePicker = false;
        this.newPrice.dateOfPrice = this.datePickerDate;
        this.displayDateString = this.datePipe.transform(this.newPrice.dateOfPrice, 'MM-dd-yyyy');
    }

    RedirectToDetails(): void{
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(this.result),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Details(navigationExtras);
    }

}
