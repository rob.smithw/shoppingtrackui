import { Component } from "@angular/core";
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import {ActivatedRoute, Router} from '@angular/router';
import { request, getJSON, getString } from "tns-core-modules/http";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Item, Details } from "~/app/shared/models/item.component";
import { DatePipe } from '@angular/common';
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Store } from "~/app/shared/models/store.component";
import { User } from "~/app/shared/models/user.component";
import { Constants } from "~/app/shared/constants/constants.component";
import { Price, PriceView } from "~/app/shared/models/price.component";

@Component({
    selector: 'prices-view',
    templateUrl: './pricesview.component.html',
    styleUrls: ['./pricesview.component.css'],
    moduleId: module.id
})


export class PricesView {

    prices: Price[];
    pricesViewModel: PriceView[];
    shared: Constants;
    result: Item;
    currentUser: User;
    currentStore: Store;
    redirect: Redirects;
    stores: Store[];

    constructor(private route: ActivatedRoute, private router: RouterExtensions, private datePipe: DatePipe){
        this.route.queryParams.subscribe(params => {
            this.result = JSON.parse(params["item"]);
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void{
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        this.getAllStores()
        this.getPrices(this.result.itemId);
    }

    getAllStores(){
        this.stores = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API + "Stores").then((r: any) => {
            this.stores = r;
        }, (e) => {
        });
    }

    getPrices(itemId: number): void{
        this.prices = [];
        this.pricesViewModel = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API+"Prices/"+itemId).then((r: any) => {
            this.prices = r;
            this.prices.forEach(price => {
                let priceView: PriceView = new PriceView(price);
                priceView.dateString = this.datePipe.transform(price.dateOfPrice, 'MM-dd-yyyy');
                if(price.storeId !== 0){
                    priceView.storeName = this.shared.getStoreNameById(price.storeId, this.stores);
                }
                else{
                    priceView.storeName = "No store listed.";
                }
                this.pricesViewModel.push(priceView);
            });
        }, (e) => {
        });
    }

}
