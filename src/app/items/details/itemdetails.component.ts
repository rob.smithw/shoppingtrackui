import { Component, Input, OnInit } from "@angular/core";
import { formatCurrency } from "@angular/common";
import {ActivatedRoute, Router} from '@angular/router';
import { request, getJSON, getString } from "tns-core-modules/http";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import {Observable} from 'rxjs';
import { Item, Details } from "~/app/shared/models/item.component";
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Constants } from "~/app/shared/constants/constants.component";
import { Price } from "~/app/shared/models/price.component";
import { Store } from "~/app/shared/models/store.component";
import { User } from "~/app/shared/models/user.component";

@Component({
    selector: 'item-details',
    templateUrl: './itemdetails.component.html',
    styleUrls: ['./itemdetails.component.css'],
    moduleId: module.id
})

export class ItemDetails {

    public showDialog: boolean;
    public nameFormatted: string;
    public priceFormatted: string;
    public previousPrices: Price[];
    public SpecificDetails: Details;
    public shared: Constants;
    public stores: Store[];
    public result: Item;
    public redirect: Redirects;
    public currentUser: User;
    public currentStore: Store;


    constructor(private route: ActivatedRoute, private router: RouterExtensions){
        this.route.queryParams.subscribe(params => {
            this.result = JSON.parse(params["item"]);
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void{
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        this.showDialog = false;
        this.GetPrices(this.result.itemId);
        this.GetAllStores();
        this.setFormattedStrings(this.result);
    }

    GetPrices(itemId: number): void{
        this.previousPrices = [];
        let cnt: number = 0;
        getJSON(this.shared.SHOPPINGTRACKTEST_API+"Prices/"+itemId).then((r: any) => {
            r.forEach(price => {
                //only get first 2 prices
                if(cnt < 2){
                    this.previousPrices.push(price);
                }
                cnt++;
            });
        }, (e) => {
        });
    }

    FormatString(type: string, toFormat: object): string{
        let stringToDisplay: string = "";
        
        if(type == 'price'){
            stringToDisplay = "Price: " + formatCurrency(Number(toFormat),"en","$");
        }
        if(type == 'previous_store' || type == 'current_store'){
            let store_type: string = "Previous Store";
            if (type == 'current_store'){
                store_type = "Current Store";
            }
            if(this.shared.isUndefinedOrNull(toFormat) || Number(toFormat) == 0){
                stringToDisplay = `${store_type}: No store specified.`;
            }
            else{
                stringToDisplay = `${store_type}: ${this.GetStoreName(Number(toFormat))}`;
            }
        }
        if(type == 'date'){
            stringToDisplay = "Date of Price: " + new Date(toFormat.toString());
        }
        
        return stringToDisplay;
    }

    GetStoreName(last_Store_Id: number): string{
        let store_name: string = "";
        if(last_Store_Id != undefined){
            this.stores.forEach(element => {
                if(element.storeId == last_Store_Id){
                    store_name = element.name;
                }
            });
        }
        return store_name;
    }

    GetAllStores(): void{
        this.stores = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API + "Stores").then((r: any) => {
            this.stores = r;
        }, (e) => {
        });
    }

    public OpenDialog(){
        this.showDialog = true;
    }

    public CloseDialog(){
        this.showDialog = false;
    }

    public DeleteItem(){
        request({
            url: this.shared.SHOPPINGTRACKTEST_API + "Items/" + this.result.itemId,
            method: "DELETE"
        }).then((response) => {
            const result = response.content.toJSON();
            let navigationExtras: NavigationExtras = {
                queryParams: {
                    "user": JSON.stringify(this.currentUser),
                    "store": JSON.stringify(this.currentStore)
                }
            };
            this.redirect.Home(navigationExtras);
        }, (e) => {
        });
    }

    public RedirectToHome(){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Home(navigationExtras);
    }

    public RedirectToAddPrice(){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(this.result),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.AddPrice(navigationExtras);
    }

    public RedirectToUpdate(){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(this.result),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Update(navigationExtras);
    }

    public RedirectToViewPrice(): void{
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(this.result),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.ViewPrice(navigationExtras);
    }

    public setFormattedStrings(resultToFormat: Item){
        this.nameFormatted = "Item Name: " + resultToFormat.name;
        if(!this.shared.isUndefinedOrNull(resultToFormat.previous_Price)){
            this.priceFormatted = "Previous Price: " + formatCurrency(resultToFormat.previous_Price,"en","$");
        }
        else{
            this.priceFormatted = "Previous Price: There was no previous price specified.";
        }
    }

}
