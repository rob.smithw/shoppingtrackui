import { Component } from "@angular/core";
import {ActivatedRoute, Router} from '@angular/router';
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { request, getJSON, getString } from "tns-core-modules/http";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Item, Details } from "~/app/shared/models/item.component";
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Store } from "../../shared/models/store.component";
import { Constants } from "~/app/shared/constants/constants.component";
import { User } from "~/app/shared/models/user.component";
import { stringify } from "@angular/compiler/src/util";

@Component({
    selector: 'item-update',
    templateUrl: './itemupdate.component.html',
    styleUrls: ['./itemupdate.component.css'],
    moduleId: module.id
})


export class ItemUpdate {

    result: Item;
    updatedItem: Item;
    last_Store_Name: string;
    current_Store_Name: string;
    redirect: Redirects;
    stores: Store[];
    shared: Constants;
    currentUser: User;
    currentStore: Store;
    showStoreSelect: boolean = false;
    storeNameList: Array<string>;
    selectedStoreInd: number = 0;
    selectedStoreName: string;
    selectedStoreType: string;
    readonly previous_store: string = 'previous_store';
    readonly current_store: string = 'current_store';

    constructor(private route: ActivatedRoute, private router: RouterExtensions){
        this.route.queryParams.subscribe(params => {
            this.result = JSON.parse(params["item"]);
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void{
        this.shared = new Constants();
        this.redirect = new Redirects(this.router);
        this.updatedItem = this.result;
        this.GetAllStores();
    }

    GetStoreName(type: string): string{
        switch (type) {
            case this.previous_store:
                let prev_store_name: string = this.shared.getStoreNameById(this.updatedItem.last_Store_Id, this.stores);
                return prev_store_name;        
            case this.current_store:
                let curr_store_name: string = this.shared.getStoreNameById(this.updatedItem.currentStoreId, this.stores);
                return curr_store_name;
            default:
                return '';
        }
    }

    GetStoreId(): number{
        let store_id: number = 0;
        if(this.last_Store_Name != undefined){
            this.stores.forEach(element => {
                if(element.name == this.last_Store_Name){
                    store_id = element.storeId;
                }
            });
        }
        return store_id;
    }

    RedirectToDetails(){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(this.result),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Details(navigationExtras);
    }

    GetAllStores(){
        this.stores = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API + "Stores").then((r: any) => {
            this.stores = r;
        }, (e) => {
        });
    }

    UpdateItem(){
        this.updatedItem.last_Store_Id = this.GetStoreId();
        let new_price: number = Number(this.updatedItem.previous_Price);
        if(this.updatedItem != undefined){
            request({
                url: this.shared.SHOPPINGTRACKTEST_API + "Items/" + this.updatedItem.itemId,
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                content: JSON.stringify({
                    itemId: this.updatedItem.itemId,
                    user_id: this.updatedItem.user_Id,
                    name: this.updatedItem.name,
                    previous_price: new_price,
                    last_store_id: this.updatedItem.last_Store_Id,
                    deleted: this.updatedItem.deleted,
                    purchased: this.updatedItem.purchased
                })
            }).then((response) => {
                let navigationExtras: NavigationExtras = {
                    queryParams: {
                        "user": JSON.stringify(this.currentUser),
                        "store": JSON.stringify(this.currentStore)
                    }
                };
                this.redirect.Home(navigationExtras);
            }, (e) => {
            });
        }
    }

    OpenStoreSelect(type: string): void{
        if(this.shared.isUndefinedOrNull(this.storeNameList) || this.storeNameList === []){
            this.CreateListWithNames();
        }
        this.selectedStoreType = type;
        switch (type) {
            case this.previous_store:
                if(!this.shared.isUndefinedOrNull(this.last_Store_Name) && this.last_Store_Name !== ''){
                    this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.last_Store_Name, this.storeNameList);
                }
                break;
            case this.current_store:
                if(!this.shared.isUndefinedOrNull(this.current_Store_Name) && this.current_Store_Name !== ''){
                    this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.current_Store_Name, this.storeNameList);
                }
                break;
            default:
                break;
        }
        this.showStoreSelect = true;
    }

    CreateListWithNames(): void{
        this.storeNameList = [];
        //initialize list
        if(!this.shared.isUndefinedOrNull(this.stores)){
            this.stores.forEach(store => {
                this.storeNameList.push(store.name);
            });
        }
        //sort list
        if(!this.shared.isUndefinedOrNull(this.storeNameList)){
            this.storeNameList = this.storeNameList.sort((first: string, second: string) => {
                if(first > second){
                    return 1;
                }
                if(first < second){
                    return -1;
                }
                return 0;
            });
        }
    }

    OnSelectedIndexChanged(args: EventData) {
        const picker = <ListPicker>args.object;
        this.selectedStoreInd = picker.selectedIndex;
        this.selectedStoreName = this.storeNameList[picker.selectedIndex];
    }

    SaveStoreSelection(): void{
        this.CloseStoreSelect();
        switch (this.selectedStoreType) {
            case this.previous_store:
                this.last_Store_Name = this.selectedStoreName;
                break;
            case this.current_store:
                this.current_Store_Name = this.selectedStoreName;
                break;
            default:
                break;
        }
    }

    CloseStoreSelect(): void{
        this.showStoreSelect = false;
    }

}
