import { Component } from "@angular/core";
import { Constants } from "../shared/constants/constants.component";
import { request, getJSON, getString } from "tns-core-modules/http";
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { formatCurrency } from "@angular/common";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras, ActivatedRoute } from "@angular/router";
import { Item, Details } from "../shared/models/item.component";
import { Redirects } from "../shared/constants/redirects.component";
import { User } from "../shared/models/user.component";
import { Store } from "../shared/models/store.component";
import { ApiService } from "../shared/services/api.service";

@Component({
    selector: 'ItemsComponent',
    templateUrl: './items.component.html',
    styleUrls: ['./items.component.css'],
    moduleId: module.id
})


export class ItemsComponent {

    shared: Constants;
    userRoute: string = "User";
    itemToSearch: string;
    exist: boolean = false;
    searchDisplay: boolean = false;
    newId: number = 0;
    estimate: number;
    estimateString: string = "";
    showDialog: boolean = false;
    itemToUpdate: Item;
    updatePrice: number;
    updateStoreName: string;
    currentUser: User;
    currentStore: Store;
    showStoreSelect: boolean = false;
    storeNameList: Array<string>;
    selectedStoreInd: number = 0;
    selectedStoreName: string;
    public redirect: Redirects;
    public ShoppingItems: Item[];
    public searchedItems: Item[];
    public purchasedItems: Item[];
    public CurrItemDetails: Details;
    public stores: Store[];
    public UserContext: User;

    constructor(private route: ActivatedRoute, private router: RouterExtensions, private api_service: ApiService){
        this.route.queryParams.subscribe(params => {
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void {
        this.redirect = new Redirects(this.router);
        this.shared = new Constants();
        this.itemToUpdate = new Item();
        this.itemToSearch = "";
        this.GetAllItems();
        this.GetAllStores();
    }

    GetEstimate(): void{
        this.estimate = 0;
        this.ShoppingItems.forEach(item => {
            this.estimate += item.previous_Price;
        });
        if(this.estimate > 0){
            this.estimateString = "Estimated price: " + formatCurrency(this.estimate,"en","$")
        }

    }

    CloseDialog(): void{
        this.showDialog = false;
    }

    RenderSearchList(searchValue: string){
        if(this.shared.isUndefinedOrNull(this.itemToSearch) || this.itemToSearch == ""){
            this.searchDisplay = false;
        }

        if(this.itemToSearch != ""){
            this.searchedItems = [];
            this.searchDisplay = true;
            if(!this.shared.isUndefinedOrNull(this.ShoppingItems)){
                this.ShoppingItems.forEach(element => {
                    if(element.name.toLowerCase().includes(this.itemToSearch.toLowerCase())){
                        this.searchedItems.push(element);
                    }
                });
            }
        }
    }

    GetAllItems(){
        this.ShoppingItems = [];
        this.purchasedItems = [];
        //if current store is not null, only get all items for specific store
        if (!this.shared.isUndefinedOrNull(this.currentStore)){
            this.api_service.getItemsForStore(this.currentStore.storeId, this.currentUser.user_Id)
                .subscribe( (response: Item[]) => {
                    response.forEach(element => {
                        if(element.purchased){
                            this.purchasedItems.push(element);
                        }
                        else{
                            this.ShoppingItems.push(element);
                        }
                    });
                    this.GetEstimate();
                },
                error => {
                    console.error(error);
                });
        }
        else{
            this.api_service.getAllItems(this.currentUser.user_Id)
                .subscribe(
                    (response: Item[]) => {
                        response.forEach(element => {
                            if(element.purchased){
                                this.purchasedItems.push(element);
                            }
                            else{
                                this.ShoppingItems.push(element);
                            }
                        });
                        this.GetEstimate();
                    },
                    error => {
                        console.error(error);
                    }
                );
        }
        
    }

    ChangePurchaseStatus(curr_item: Item, purchased_status: boolean): void{
        if(!this.shared.isUndefinedOrNull(purchased_status)){
            if(purchased_status){
                this.showDialog = true;
                this.itemToUpdate = curr_item;
                this.updatePrice = this.itemToUpdate.previous_Price;
                this.updateStoreName = this.GetStoreName(this.itemToUpdate.last_Store_Id);
            }
            curr_item.purchased = purchased_status;
            request({
                url: this.shared.SHOPPINGTRACKTEST_API + "Items/" + curr_item.itemId,
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                content: JSON.stringify({
                    itemId: curr_item.itemId,
                    user_id: curr_item.user_Id,
                    name: curr_item.name,
                    previous_price: curr_item.previous_Price,
                    last_store_id: curr_item.last_Store_Id,
                    deleted: curr_item.deleted,
                    purchased: curr_item.purchased
                })
            }).then((response) => {
                //not ordered by name, may want to look into this in the future
                if(purchased_status){
                    this.ShoppingItems = this.ShoppingItems.filter(item => item.itemId != curr_item.itemId);
                    this.purchasedItems.push(curr_item);
                }
                if(!purchased_status){
                    this.purchasedItems = this.purchasedItems.filter(item => item.itemId != curr_item.itemId);
                    this.ShoppingItems.push(curr_item);
                }
            }, (e) => {
            });
        }
    }

    UpdatePricePaid(store_name: string, new_price_str: number): void{
        if(!this.shared.isUndefinedOrNull(this.itemToUpdate)){
            //store the previous price in Prices table
            if(!this.shared.isUndefinedOrNull(this.itemToUpdate.previous_Price) && !this.shared.isUndefinedOrNull(this.itemToUpdate.last_Store_Id)){
                request({
                    url: this.shared.SHOPPINGTRACKTEST_API + "Prices",
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    content: JSON.stringify({
                        itemId: this.itemToUpdate.itemId,
                        userId: this.itemToUpdate.user_Id,
                        price: this.itemToUpdate.previous_Price,
                        storeId: this.itemToUpdate.last_Store_Id,
                        dateOfPrice: new Date()
                    })
                }).then((response) => {
                }, (e) => {
                });
            }

            //update the new price to the new previous price and new store id to store id
            this.itemToUpdate.last_Store_Id = this.GetStoreId(store_name);
            let new_price: number = Number(new_price_str);
            this.itemToUpdate.previous_Price = new_price;
            request({
                url: this.shared.SHOPPINGTRACKTEST_API + "Items/" + this.itemToUpdate.itemId,
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                content: JSON.stringify({
                    itemId: this.itemToUpdate.itemId,
                    user_id: this.itemToUpdate.user_Id,
                    name: this.itemToUpdate.name,
                    previous_price: new_price,
                    last_store_id: this.itemToUpdate.last_Store_Id,
                    deleted: this.itemToUpdate.deleted,
                    purchased: this.itemToUpdate.purchased
                })
            }).then((response) => {
            }, (e) => {
            });
        }
        this.showDialog = false;
    }

    GetStoreId(store_name: string): number{
        let store_id: number = 0;
        if(!this.shared.isUndefinedOrNull(store_name)){
            this.stores.forEach(element => {
                if(element.name == store_name){
                    store_id = element.storeId;
                }
            });
        }
        return store_id;
    }

    GetStoreName(last_Store_Id: number): string{
        let store_name: string = "";
        if(last_Store_Id != undefined){
            this.stores.forEach(element => {
                if(element.storeId == last_Store_Id){
                    store_name = element.name;
                }
            });
        }
        return store_name;
    }

    RedirectToAdd(): void{
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.AddPage(navigationExtras);
    }

    GetAllStores(){
        this.stores = [];
        getJSON(this.shared.SHOPPINGTRACKTEST_API + "Stores").then((r: any) => {
            this.stores = r;
        }, (e) => {
        });
    }

    RenderDetails(curr_item: Item){
        console.log(this.currentStore);
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "item": JSON.stringify(curr_item),
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Details(navigationExtras);
    }

    OpenStoreSelect(): void{
        if(this.shared.isUndefinedOrNull(this.storeNameList) || this.storeNameList === []){
            this.CreateListWithNames();
        }
        if(!this.shared.isUndefinedOrNull(this.updateStoreName) && this.updateStoreName !== ''){
            this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.updateStoreName, this.storeNameList);
        }
        this.showStoreSelect = true;
    }

    CreateListWithNames(): void{
        this.storeNameList = [];
        //initialize list
        if(!this.shared.isUndefinedOrNull(this.stores)){
            this.stores.forEach(store => {
                this.storeNameList.push(store.name);
            });
        }
        //sort list
        if(!this.shared.isUndefinedOrNull(this.storeNameList)){
            this.storeNameList = this.storeNameList.sort((first: string, second: string) => {
                if(first > second){
                    return 1;
                }
                if(first < second){
                    return -1;
                }
                return 0;
            });
        }
    }

    OnSelectedIndexChanged(args: EventData) {
        const picker = <ListPicker>args.object;
        this.selectedStoreInd = picker.selectedIndex;
        this.selectedStoreName = this.storeNameList[picker.selectedIndex];
    }

    SaveStoreSelection(): void{
        this.CloseStoreSelect();
        this.updateStoreName = this.selectedStoreName;
    }

    CloseStoreSelect(): void{
        this.showStoreSelect = false;
    }

}
