import { Component } from "@angular/core";
import {ActivatedRoute, Router} from '@angular/router';
import { EventData } from "tns-core-modules/data/observable";
import { ListPicker } from "tns-core-modules/ui/list-picker";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras } from "@angular/router";
import { Item, Details } from "~/app/shared/models/item.component";
import { Redirects } from "~/app/shared/constants/redirects.component";
import { Store } from "../../shared/models/store.component";
import { User } from "~/app/shared/models/user.component";
import { Constants } from "~/app/shared/constants/constants.component";
import { ApiService } from "~/app/shared/services/api.service";

@Component({
    selector: 'item-add',
    templateUrl: './itemadd.component.html',
    styleUrls: ['./itemadd.component.css'],
    moduleId: module.id
})


export class ItemAdd {

    result: Item;
    itemToAdd: Item;
    current_store_name: string;
    redirect: Redirects;
    errorMessage: string;
    showStoreSelect: boolean = false;
    storeNameList: Array<string>;
    selectedStoreInd: number = 0;
    selectedStoreName: string;
    stores: Store[];
    currentUser: User;
    currentStore: Store;
    all_items: Item[];
    shared: Constants;

    constructor(private route: ActivatedRoute, private router: RouterExtensions, private api_service: ApiService){
        this.route.queryParams.subscribe(params => {
            this.currentUser = JSON.parse(params["user"]);
            this.currentStore = JSON.parse(params["store"]);
        });
    }

    ngOnInit(): void{
        this.shared = new Constants();
        this.itemToAdd = new Item(this.currentUser.user_Id);
        this.redirect = new Redirects(this.router);
        this.GetAllItemsForUser(this.currentUser.user_Id);
        this.GetAllStores();
        this.SetCurrentStore();
    }

    SetCurrentStore(): void {
        if (!this.shared.isUndefinedOrNull(this.currentStore)){
            this.current_store_name = this.currentStore.name;
        }
    }

    GetAllStores(){
        this.stores = [];
        this.api_service.getAllStores()
            .subscribe(
                (response: Store[]) => {
                    this.stores = response;
                },
                error => {
                    console.error(error);
                }
            );
    }

    AddShoppingItem(){
        if(this.itemToAdd.name != ""){
            if(!this.ItemInList(this.itemToAdd.name)){
                this.itemToAdd.currentStoreId = this.shared.getStoreId(this.current_store_name, this.stores);
                this.api_service.addItem(this.itemToAdd)
                    .subscribe(
                        (item: Item) => {
                            this.RedirectToHome();
                        },
                        error => {
                            console.error(error);
                        }
                    );
            }
            else{
                this.errorMessage = "Item already exist.";
            }
        }
    }

    ItemInList(value: string): boolean{
        let isInList: boolean = false;
        this.all_items.forEach(element => {
            if(element["name"] == value){
                isInList = true;
            }
        });
        return isInList
    }

    GetAllItemsForUser(user_id: number){
        this.api_service.getAllItems(user_id)
            .subscribe(
                (response: Item[]) => {
                    this.all_items = response;
                },
                error => {
                    console.error(error);
                }
            );
    }

    RedirectToHome(){
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "user": JSON.stringify(this.currentUser),
                "store": JSON.stringify(this.currentStore)
            }
        };
        this.redirect.Home(navigationExtras);
    }


    OpenStoreSelect(): void{
        if(this.shared.isUndefinedOrNull(this.storeNameList) || this.storeNameList === []){
            this.CreateListWithNames();
        }
        if(!this.shared.isUndefinedOrNull(this.current_store_name) && this.current_store_name !== ''){
            this.selectedStoreInd = this.shared.getIndexOfNameFromList(this.current_store_name, this.storeNameList);
        }
        this.showStoreSelect = true;
    }

    CreateListWithNames(): void{
        this.storeNameList = [];
        //initialize list
        if(!this.shared.isUndefinedOrNull(this.stores)){
            this.stores.forEach(store => {
                this.storeNameList.push(store.name);
            });
        }
        //sort list
        if(!this.shared.isUndefinedOrNull(this.storeNameList)){
            this.storeNameList = this.storeNameList.sort((first: string, second: string) => {
                if(first > second){
                    return 1;
                }
                if(first < second){
                    return -1;
                }
                return 0;
            });
        }
    }

    OnSelectedIndexChanged(args: EventData) {
        const picker = <ListPicker>args.object;
        this.selectedStoreInd = picker.selectedIndex;
        this.selectedStoreName = this.storeNameList[picker.selectedIndex];
    }

    SaveStoreSelection(): void{
        this.CloseStoreSelect();
        this.current_store_name = this.selectedStoreName;
    }

    CloseStoreSelect(): void{
        this.showStoreSelect = false;
    }

}
