import { ItemsComponent } from "./items/items.component";
import { ItemDetails } from "./items/details/itemdetails.component";
import { ItemUpdate } from "./items/update/itemupdate.component";
import { ItemAdd } from "./items/add/itemadd.component";
import { PricesAdd } from "./items/Prices/add/pricesadd.component";
import { PricesView } from "./items/Prices/view/pricesview.component";
import { LoginComponent } from "./login/login.component";
import { StoreSelect } from "./store_selection/storeSelection.component"

export const appRoutes: any = [
    { path: "", component: LoginComponent},
    { path: "Items", component: ItemsComponent},
    { path: "ItemDetails", component: ItemDetails},
    { path: "ItemUpdate", component: ItemUpdate },
    { path: "ItemAdd", component: ItemAdd },
    { path: "PricesAdd", component: PricesAdd },
    { path: "PricesView", component: PricesView },
    { path: "Store_Select", component: StoreSelect}
];

export const appComponents: any = [
    LoginComponent,
    ItemsComponent,
    ItemDetails,
    ItemUpdate,
    ItemAdd,
    PricesAdd,
    PricesView,
    StoreSelect
];
