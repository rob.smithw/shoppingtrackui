import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { NavigationExtras, Params } from "@angular/router";

@Component({
    selector: 'redirects',
    moduleId: module.id
})

export class Redirects {
    public router: RouterExtensions;

    constructor(suppliedRouter: RouterExtensions){
        this.router = suppliedRouter;
    }

    Login(){
        this.router.navigate([""]);
    }

    Home(navigationExtras: NavigationExtras){
        this.router.navigate(["Items"], navigationExtras);
    }

    StoreSelect(navigationExtras: NavigationExtras){
        this.router.navigate(["Store_Select"], navigationExtras);
    }

    Update(navigationExtras: NavigationExtras){
        this.router.navigate(["ItemUpdate"], navigationExtras);
    }

    Details(navigationExtras: NavigationExtras){
        this.router.navigate(["ItemDetails"], navigationExtras);
    }

    AddPage(navigationExtras: NavigationExtras){
        this.router.navigate(["ItemAdd"], navigationExtras);
    }

    AddPrice(navigationExtras: NavigationExtras){
        this.router.navigate(["PricesAdd"], navigationExtras);
    }

    ViewPrice(navigationExtras: NavigationExtras){
        this.router.navigate(["PricesView"], navigationExtras)
    }

    SetUpNavigationExtras(params: Record<string, object>): NavigationExtras{
        let keys: string[] = Object.keys(params);
        let navigationExtras: NavigationExtras = {
            queryParams: { }
        };
        keys.forEach(key => {
            let param: Params = {
                key: params[key]
            };
            navigationExtras.queryParams.Add(param);
        });
        return navigationExtras;
    }

}
