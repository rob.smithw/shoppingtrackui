import { Component } from "@angular/core";
import { Store } from "../models/store.component";
import { getJSON } from "tns-core-modules/http/http";

@Component({
    selector: 'constants',
    moduleId: module.id
})

export class Constants {

    public readonly SHOPPINGTRACKTEST_API: string;
    public readonly SHOPPINGTRACKPROD_API: string;
    public readonly LOCAL_API: string;

    constructor(){
        this.SHOPPINGTRACKTEST_API = "http://10.0.2.2:80/api/";
        this.SHOPPINGTRACKPROD_API = "http://45.79.198.133:8080/api/";
        this.LOCAL_API = "https://localhost:44358/api/";
    }

    // Store Helper Functions

    public getAllStores(stores: Store[]): void{
        stores = []
        if(!this.isUndefinedOrNull(this.SHOPPINGTRACKTEST_API)){
            getJSON(this.SHOPPINGTRACKTEST_API + "Stores")
            .then((r: any) => {
                stores = r;
            }, (e) => {
            });
        }
    }

    public getStoreIdByName(name: string, allStores: Store[]): number{
        let storeId: number = 0;
        allStores.forEach(store => {
            if(store.name === name){
                storeId = store.storeId;
            }
        });
        return storeId;
    }

    public getStoreNameById(id: number, allStores: Store[]): string{
        let storeName: string;
        allStores.forEach(store => {
            if(store.storeId === id){
                storeName = store.name;
            }
        });
        return storeName;
    }

    public getStoreId(store_name: string, store_list: Store[]): number{
        let store_id: number = 0;
        if(store_name != undefined){
            store_list.forEach(element => {
                //case insensitive
                if(element.name.toLocaleLowerCase() == store_name.toLocaleLowerCase()){
                    store_id = element.storeId;
                }
            });
        }
        return store_id;
    }

    // General Helper Functions

    public getIndexOfNameFromList(name: string, list: string[]): number{
        let index: number = 0;
        if(!this.isUndefinedOrNull(list)){
            index = list.indexOf(name);
            if(index > -1){
                return index;
            }
            else{
                index = 0;
                return index;
            }
        }
        return index;
    }

    //creating my own as this does not exist in typescript currently
    public isUndefinedOrNull(value: any): boolean{
        if(value == null){
            return true;
        }

        if(value === null){
            return true;
        }

        if(typeof value === 'undefined'){
            return true;
        }

        return false;
    }
}
