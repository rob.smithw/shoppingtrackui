import { Component } from "@angular/core";

@Component({
    selector: 'store',
    moduleId: module.id
})

export class Store {
    public storeId: number
    public name: string
}
