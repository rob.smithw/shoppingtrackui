import { Component } from "@angular/core";
import { formatCurrency } from "@angular/common";

@Component({
    selector: 'price',
    moduleId: module.id
})


export class Price {
    public id: number;
    public itemId: number;
    public userId: number;
    public price: number;
    public storeId: number;
    public dateOfPrice: Date;

    constructor(){
        this.id = 0
        this.itemId = 0;
        this.userId = 0;
        this.price = 0;
        this.storeId = 0;
        this.dateOfPrice = new Date();
    }
}

export class PriceView {
    public dateString: string;
    public storeName: string;
    public priceString: string;

    constructor(priceArg: Price){
        this.dateString = '';
        this.storeName = '';
        this.priceString = formatCurrency(priceArg.price,"en","$");
    }
}
