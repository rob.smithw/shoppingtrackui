import { Component } from "@angular/core";

@Component({
    selector: 'item',
    moduleId: module.id
})


export class Item {
    public itemId: number;
    public user_Id: number;
    public name: string;
    public previous_Price: number;
    public last_Store_Id: number;
    public deleted: boolean;
    public purchased: boolean;
    public currentStoreId: number;

    constructor(user_id?: number){
        this.itemId = 0;
        this.user_Id = user_id || 0;
        this.name = "";
        this.previous_Price = 0;
        this.last_Store_Id = 0;
        this.deleted = false;
        this.purchased = false;
        this.currentStoreId = 0;
    }

}

export interface Details {
    name: string,
    previous_price:number,
    last_store_id:number,
    currentStoreId: number
}
