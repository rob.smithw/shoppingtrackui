import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Constants } from '../constants/constants.component';
import { Store } from '../models/store.component';
import { Item } from '../models/item.component';
import { User } from '../models/user.component';

@Injectable()
export class ApiService {

    constructor(private httpClient: HttpClient) {
    }

    private constants = new Constants();
    private readonly API_URL = this.constants.SHOPPINGTRACKTEST_API;
    private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    public errorHandler(error: HttpErrorResponse){
        if (error.error instanceof HttpErrorResponse) {
        // A client-side or network error occurred.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
            console.log(error);
        }
        // Return an observable with a error message.
        return throwError('Error occured; please try again later.');
    }

    // Store endpoints

    getStoresWithItems(user_id: number): Observable<Store[]> {
        return this.httpClient.get<Store[]>(`${this.API_URL}Stores/GetStoresWithItemsByUser?userId=${user_id}`)
            .pipe(catchError(this.errorHandler));
    }
    getAllStores(): Observable<Store[]> {
        return this.httpClient.get<Store[]>(`${this.API_URL}Stores`)
            .pipe(catchError(this.errorHandler));
    }

    // Item endpoints

    getItemsForStore(store_id: number, user_id: number): Observable<Item[]> {
        return this.httpClient.get<Item[]>(`${this.API_URL}Items/GetItemsByStoreId?storeId=${store_id}&userId=${user_id}`)
            .pipe(catchError(this.errorHandler));
    }
    getAllItems(user_id: number): Observable<Item[]> {
        return this.httpClient.get<Item[]>(`${this.API_URL}Items?user_id=${user_id}`)
            .pipe(catchError(this.errorHandler));
    }
    addItem(item: Item): Observable<Item> {
        return this.httpClient.post<Item>(`${this.API_URL}Items`, item, this.options)
            .pipe(catchError(this.errorHandler));
    }

    // User endpoints

    login(user: User): Observable<User> {
        return this.httpClient.post<User>(`${this.API_URL}User/Login`, user, this.options)
            .pipe(catchError(this.errorHandler));
    }
    registerUser(user: User): Observable<User> {
        return this.httpClient.post<User>(`${this.API_URL}User/Register`, user, this.options)
            .pipe(catchError(this.errorHandler));
    }
}
